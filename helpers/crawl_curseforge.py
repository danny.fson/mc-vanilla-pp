#!/bin/env python3

# Usage: ./crawl_curseforge.py -h

from pyquery import PyQuery as pq
import logging
import re
import argparse
import json
import sys
from selenium import webdriver


def get(url):
    tmp_no_reload = False

    while True:
        if not tmp_no_reload:
            ff.get(url)
        else:
            tmp_no_reload = False

        html = ff.find_element_by_xpath("//*").get_attribute('outerHTML')

        if 'Attention Required!' in pq(html).find('title').text():
            logger.warning('User interaction required, press enter')
            input()

            tmp_no_reload = True
            continue
        return html


parser = argparse.ArgumentParser(description='Crawls links from curseforge for mod data to stdout')
parser.add_argument('link', metavar='URL', type=str, nargs='+', help='Links to curseforge')
parser.add_argument('-p', '--profile', type=str, help='Path to FF profile to be used')
parser.add_argument('-o', '--output', type=str, help='Path to output file, will try to continue if not empty, default is /dev/stdout')
parser.add_argument('-v', '--verbose', action="store_true", help='Enable debug logging to stderr')

args = parser.parse_args()


logging.basicConfig()
logger = logging.getLogger(__name__)
if args.verbose:
    logger.setLevel(logging.DEBUG)


logger.debug(args)


if args.profile:
    ff = webdriver.Firefox(webdriver.FirefoxProfile(args.profile))
else:
    ff = webdriver.Firefox()


links = []
for link in args.link:
    offset = 0
    if 'curseforge.com' in link:
        offset = link.find('curseforge.com') + 14
    links.append(link[offset:])
visited = []


output_file = sys.stdout
if args.output:
    try:
        output_file = open(args.output, 'r+')
    except FileNotFoundError:
        output_file = open(args.output, 'w+')
    prior_output = output_file.read()[:-4]

    output_file.seek(0)

    if prior_output.strip(' \n') != '':
        prior_output += (prior_output.count('{') - prior_output.count('}')) * '}'
        prior = json.loads(prior_output)

        for name in prior.keys():
            visited.append(f'/minecraft/mc-mods/{name}')

        print(json.dumps(prior, indent=4)[:-2] + ',\n', file=output_file)
    else:
        print('{', file=output_file)
else:
    print('{', file=output_file)


while len(links) > 0:
    link = links.pop()

    logger.debug(link)

    if re.match(r'\/minecraft\/modpacks\/([\w-]+).*', link):  # modpack link
        modpack_name = re.findall(r'\/minecraft\/modpacks\/([\w-]+).*', link)[0]

        logger.debug(f'Found modpack: "{modpack_name}"')

        page = pq(get(f'https://www.curseforge.com/minecraft/modpacks/{modpack_name}/relations/dependencies'))

        page_count = 1
        if page.find('a.pagination-item'):
            page_count = int(page.find('a.pagination-item')[-1].text_content())

        for i in range(page_count):
            page = pq(get(f'https://www.curseforge.com/minecraft/modpacks/{modpack_name}/relations/dependencies?page={i+1}'))

            for a in page.find('li.project-listing-row > div:nth-child(2) > div:nth-child(1) > a:nth-child(1)'):
                links.append(a.attrib['href'])
    # elif re.match  # modpack search
    #     dostuff()
    # elif re.match(r'\/minecraft\/mc-mods\/search/\?search=([\w- ]).*', link):  # mod tag serach
    #     query = re.findall(r'\/minecraft\/mc-mods\/search/\?search=([\w- ]).*', link)[0]
    #
    #     page = pq(get(f'https://www.curseforge.com/minecraft/mc-mods/search?search={query}&page={i+1}'))
    #
    #     page_count = 1
    #     if page.find('a.pagination-item'):
    #         page_count = int(page.find('a.pagination-item')[-1].text_content())
    #
    #     for i in range(page_count):
    #         page = pq(get(f'https://www.curseforge.com/minecraft/modpacks/{modpack_name}/relations/dependencies?page={i+1}'))
    #
    #         for a in page.find('li.project-listing-row > div:nth-child(2) > div:nth-child(1) > a:nth-child(1)'):
    #             links.append(a.attrib['href'])
    elif re.match(r'\/minecraft\/mc-mods\/([A-Za-z0-9-_\/]+).*', link):  # mod/tag link
        tag = re.findall(r'\/minecraft\/mc-mods\/([A-Za-z0-9-_\/]+).*', link)[0]

        page = pq(get(f'https://www.curseforge.com/minecraft/mc-mods/{tag}'))

        if page.find('body.body-publicproject-listing'):
            logger.debug(f'Found tag: "{tag}"')

            page_count = 1
            if page.find('a.pagination-item'):
                page_count = int(page.find('a.pagination-item')[-1].text_content())

            for i in range(page_count):
                page = pq(get(f'https://www.curseforge.com/minecraft/mc-mods/{tag}?page={i+1}'))

                for a in page.find('div.px-2:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) a.my-auto:first-child'):
                    links.append(a.attrib['href'])
        else:
            mod_name = tag

            logger.debug(f'Found mod: "{mod_name}"')

            mod_info = {'slug': mod_name,
                        'name': page.find('h2.font-bold').text(),
                        'description': page.find('.pb-2').html(),
                        'latest_versions': [e.text.strip(' \n').split(' ')[1] for e in page.find('h4.e-sidebar-subheader > a:nth-child(1)')],
                        'id': int(page.find('div.mb-3:nth-child(2) > div:nth-child(1) > span:nth-child(2)').text()),
                        'source': page.find('#nav-source-svg-class-icon-icon-offsite-nav-viewbox-0-0 > a:nth-child(1)').attr('href'),
                        'tags': [e.attrib['href'][19:] for e in page.find('div.-mx-1:nth-child(2) > div > a:nth-child(1)')],
                        'latest_files': {e.text: e.attrib['href'] for e in page.find('.cf-sidebar-inner a.truncate')},
                        'licence': {e.text.strip(' \n'): e.attrib['href'] for e in page.find('div.w-full:nth-child(5) > a:nth-child(2)')},
                        'dependencies': []}

            page = pq(get(f'https://www.curseforge.com/minecraft/mc-mods/{mod_name}/relations/dependencies'))
            page_count = 1
            if page.find('a.pagination-item'):
                page_count = int(page.find('a.pagination-item')[-1].text_content())

            for i in range(page_count):
                page = pq(get(f'https://www.curseforge.com/minecraft/mc-mods/{mod_name}/relations/dependencies?page={i+1}'))

                for a in page.find('li.project-listing-row > div:nth-child(2) > div:nth-child(1) > a:nth-child(1)'):
                    links.append(a.attrib['href'])

                    mod_info['dependencies'].append(a.attrib['href'].split('/')[-1])

            json_data = "\n    ".join(json.dumps(mod_info, indent=4).split("\n"))
            print(f'    "{mod_name}": {json_data},', file=output_file)
    else:
        logger.error(f'"{link}" has unknown type')

    visited.append(link)
    links = list(set(links) - set(visited))  # remove duplicates and already visited

print('}', file=output_file)
output_file.close()
ff.close()
